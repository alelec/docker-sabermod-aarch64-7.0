---------------------------------
Docker sabermod aarch64 toolchain
---------------------------------

Docker image for use in CI builds android kernels for 64 bit nougat devices using sabermod toolchain

More info at:
* https://bitbucket.org/xanaxdroid/aarch64-7.0
