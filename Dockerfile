FROM dockerimages/ubuntu-core
MAINTAINER https://gitlab.com/alelec/docker-sabermod-aarch64-7.0

# Enable universe and update 
RUN sed -ri 's/^# *(.* universe)/\1/g' /etc/apt/sources.list
RUN apt update

# Install toolchain and associated requirements
RUN apt install build-essential bc git-core python python-pip python3 python3-pip openssh-client p7zip-full curl && \
    pip install -U pip setuptools wheel && \
    pip3 install -U pip setuptools wheel
RUN git -c http.sslVerify=false clone https://bitbucket.org/xanaxdroid/aarch64-7.0.git

# Print out installed version
RUN VERSION=$(sed -rn 's/^.*([0-9]+\.[0-9]+\..+-sabermod) ([0-9]+).*$/\1_\2/p' aarch64-7.0/VERSION); \
   echo "VERSION=$VERSION"

ENV CROSS_COMPILE=$(pwd)/aarch64-7.0/bin/aarch64-

# Clean up cache
RUN rm -rf /var/cache/apk/*